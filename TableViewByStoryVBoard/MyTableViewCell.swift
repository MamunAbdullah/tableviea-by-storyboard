//
//  MyTableViewCell.swift
//  TableViewByStoryVBoard
//
//  Created by Workspace Infotech on 9/6/18.
//  Copyright © 2018 Workspace Infotech. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet var img: UIImageView!
    @IBOutlet var lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
