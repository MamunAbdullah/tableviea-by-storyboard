//
//  ViewController.swift
//  TableViewByStoryVBoard
//
//  Created by Workspace Infotech on 9/6/18.
//  Copyright © 2018 Workspace Infotech. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
var name = ["Mmaun","Mmaun1","Mmaun2","Mmaun3","Mmaun4","Mmaun5","Mmaun6","Mmaun7"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MyTableViewCell
        cell?.lbl.text = name[indexPath.row]
        cell?.img.image = UIImage(named: name[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detVc = storyboard?.instantiateViewController(withIdentifier: "detailsViewController") as? DetailsViewController
        detVc?.dtimage = UIImage(named: name[indexPath.row])!
        detVc?.dtname = name[indexPath.row]
        self.navigationController?.pushViewController(detVc!, animated: true)
    }
    

}

